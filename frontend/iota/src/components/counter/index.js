import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';

import * as styles from "./styles";
import {Link} from 'react-router-dom';

import axios from 'axios';

import {
  COUNTER_URL,
  } from '../../api/constant';

class CounterComponent extends Component {

  constructor(props) {
    super(props);
    this.state = { total: null,
                   form: {
                     value: null,
                     operator: "+"
                   }
                 };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeGeneric = this.handleChangeGeneric.bind(this);
    this.handleChangeValue = this.handleChangeValue.bind(this);
    this.gotoHistory= this.gotoHistory.bind(this);
  }

   handleSubmit(event) {
    axios.post(COUNTER_URL,
       this.state.form
    ).then(response => response.data)
    .then((data) => {
      this.setState({ total: data.total})
     })
    event.preventDefault();
  }

  handleChangeGeneric(event) {
    this.setState({ form: { ...this.state.form, [event.target.name]: event.target.value} });
  }

  handleChangeValue(event) {
    const onlyNums = event.target.value.replace(/[^0-9]/g, '');
    this.setState({ form: { ...this.state.form, value:  onlyNums }});
  }

 gotoHistory(){
  let { history } = this.props;
  history.push({
   pathname: '/history'
  });
 }

  componentDidMount() {
    axios.get(COUNTER_URL).then(response => response.data)
    .then((data) => {
      this.setState({ total: data.total})
      console.log(this.state.total)
     })
  }

  render() {
    return <div style={styles.ModalStyle}>
        <div className="Header">
           <Link to="/history">
             <Button style={styles.CustomButtonStyle}>
                ACCEDER A L'HISTORIQUE
             </Button>
           </Link>
        </div>
        <p style={styles.TotalIndicatorStyle}>
         {this.state.total}
        </p>
        <form autoComplete="off" onSubmit={this.handleSubmit} style={styles.flexContainerStyle} >
          <FormControl style={styles.SpacingFormControlStyle}>
            <Select
              value= {this.state.form.operator}
              onChange={this.handleChangeGeneric}
              name='operator'
            >
              <MenuItem value='+'>Addition <Icon>AddCircle</Icon></MenuItem>
              <MenuItem value='-'>Soustraction <Icon>RemoveCircle</Icon></MenuItem>
              <MenuItem value='/'>Division <Icon>Error</Icon></MenuItem>
              <MenuItem value='*'>Multiplication <Icon>Cancel</Icon></MenuItem>
            </Select>
          </FormControl>
          <FormControl style={styles.SpacingFormControlStyle}>
          <TextField
                id="value"
                label="Valeur"
                name="value"
                onChange={this.handleChangeValue}
                value= {this.state.form.value}
                margin="normal"
                variant="outlined"
              />
           </FormControl>
           <FormControl style={styles.SpacingFormControlStyle}>
           <Button type="submit" style={styles.CustomButtonStyle} >
                  Valider
             </Button>
            </FormControl>
        </form>

    </div>;
  }
}

export default CounterComponent;