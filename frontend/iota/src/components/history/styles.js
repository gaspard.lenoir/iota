export const ModalStyle = {
  background: "white",
  margin: "auto auto",
  minHeight: "760px",
  width: "670px",
  boxShadow: "3px 3px 2px #00000029",
  borderRadius: "5px",
  textAlign:"left",
  padding:"10px"
}
