import React from 'react';
import NotFound from '../components/notfound';

function App() {
  return (
    <div>
      <NotFound/>
    </div>
  );
}

export default App;
