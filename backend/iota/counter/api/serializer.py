from rest_framework import serializers

from counter.models import CounterStep, OPERATORS


class CounterSerializer(serializers.ModelSerializer):

    class Meta:
        model = CounterStep
        fields = ['total', 'value', 'operator']
        write_only_fields = ('value', 'operator')
        extra_kwargs = {
            'value': {'write_only': True},
            'operator': {'write_only': True},
            'total': {'read_only': True},
        }

    def validate_operator(self, operator):
        if operator not in OPERATORS:
            raise serializers.ValidationError(
                f'Operator is not in {OPERATORS}'
            )
        return operator

    def create(self, validated_data):
        return CounterStep.objects.create(**validated_data)


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = CounterStep
        fields = ['value', 'operator', 'total']
