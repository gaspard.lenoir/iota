
export const flexContainerStyle = {
  display: 'flex',
  margin: 'auto',
  width:  '330px',
  flexDirection: 'column',
};

export const SpacingFormControlStyle = {
  padding: '15px 0'
}

export const TotalIndicatorStyle ={
  letterSpacing: 0,
  fontSize: "181px",
  fontWeight: '150px',
  margin:'0px',
  color: "#6100ED"
}

export const CustomButtonStyle = {
  backgroundColor: "#6100ED",
  width: "100%",
  margin: "-11",
  letterSpacing: "1.25px",
  color: "#FFFFFF",
  textAlign: 'right',
  boxShadow: '0px 6px 10px #00000029'
}

export const ModalStyle = {
  background: "white",
  margin: "auto auto",
  minHeight: "760px",
  width: "670px",
  boxShadow: "3px 3px 2px #00000029",
  borderRadius: "5px"
}
