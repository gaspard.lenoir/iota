from django.db import models
from django.utils import timezone

OPERATORS = ['+', '-', '/', '*']

class StepManager():

    @staticmethod
    def execute_calculation(a: int, b: int, operator: OPERATORS) -> int:
        if operator is '+':
            return a + b
        elif operator is '-':
            return a - b
        elif operator is '/':
            return int(a / b)
        elif operator is '*':
            return a * b
        else:
            raise ValueError('could not find %c in %s' % (operator, OPERATORS))

    @staticmethod
    def get_latest_or_empty_step():
        try:
            return CounterStep.objects.latest('created_at')
        except CounterStep.DoesNotExist:
            return CounterStep()


class CounterStep(models.Model):

    value = models.IntegerField(db_column="value")
    operator = models.TextField(db_column="operator")
    total = models.IntegerField(null=False, default=0, db_column="total")
    created_at = models.DateTimeField(
        null=False, default=timezone.now, db_column="created_at"
    )

    class Meta:
        db_table = "counter_step"

    def save( self, *args, **kw ):

        previous_step = StepManager.get_latest_or_empty_step()

        previous_total = previous_step.total

        self.total = StepManager.execute_calculation(previous_total, self.value, self.operator)

        super(CounterStep, self).save(*args, **kw)

    def __str__(self):
        return f'<CounterStep object : value={self.value} operator={self.operator} total={self.total}>'
