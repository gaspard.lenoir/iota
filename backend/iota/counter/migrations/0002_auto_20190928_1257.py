# Generated by Django 2.2.5 on 2019-09-28 12:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('counter', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='counterstep',
            table='counter_step',
        ),
    ]
