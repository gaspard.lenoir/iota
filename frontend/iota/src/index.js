import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import {BrowserRouter, Switch, Route} from 'react-router-dom';
import HomeContainer from './containers/home';
import HistoryContainer from './containers/history';
import NotFoundPage from './containers/notfoundpage';

ReactDOM.render(
<BrowserRouter>
 <div className="app-layout">
    <Switch>
      <Route path="/history" component={HistoryContainer} />
      <Route exact path="/" component={HomeContainer} />
      <Route component={NotFoundPage} />
    </Switch>
  </div>
</BrowserRouter>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
