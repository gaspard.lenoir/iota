from collections import OrderedDict
from unittest import TestCase

from rest_framework.test import APITestCase

from counter.models import CounterStep, StepManager


class TestCalculation(TestCase):

    def test_calculation(self):
        self.assertEqual(StepManager.execute_calculation(12, 12, "+"), 24)
        self.assertEqual(StepManager.execute_calculation(12, 12, "-"), 0)
        self.assertEqual(StepManager.execute_calculation(12, 2, "*"), 24)
        self.assertEqual(StepManager.execute_calculation(12, 5, "/"), 2)

        with self.assertRaises(ValueError):
            StepManager.execute_calculation(12, 5, "R")


class TestCounterApi(APITestCase):

    def test_step_object(self):
        family = CounterStep.objects.create(
            value=12,
            operator='+'
        )
        family2 = CounterStep.objects.create(
            value=12,
            operator='+'
        )
        self.assertEqual(str(family), "<CounterStep object : value=12 operator=+ total=12>")
        self.assertEqual(str(family2), "<CounterStep object : value=12 operator=+ total=24>")


    def test_get_counter(self):
        response = self.client.get("/api/counter/")

        self.assertEqual(
            response.data,
            {'total': 0}
        )

    def test_post_counter(self):
        response = self.client.post(
            "/api/counter/", {'value':'12',
            'operator':'+'}
        )
        self.assertEqual(response.status_code, 201)

    def test_post_counter_wrong_operator(self):
        response = self.client.post(
            "/api/counter/", {'value':'12',
            'operator':'T'}
        )

        self.assertEqual(response.status_code, 400)

    def test_get_history(self):

        CounterStep.objects.create(
            value=12,
            operator='+'
        )

        response = self.client.get("/api/history/")
        self.assertEqual(
            response.data,
            [OrderedDict([('value', 12), ('operator', '+'), ('total', 12)])]
        )

    def test_delete_history(self):

        CounterStep.objects.create(
            value=12,
            operator='+'
        )

        response = self.client.delete("/api/history/")

        self.assertEqual(
            response.status_code,
            204,  f"Error : {response}"
        )

        response = self.client.get("/api/history/")

        self.assertEqual(
            response.data,
            []
        )