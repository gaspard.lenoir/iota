import React, {Component} from 'react';

import * as styles from "./styles";

import axios from 'axios';

import {
  HISTORY_URL,
  } from '../../api/constant';


class HistoryComponent extends Component {

  constructor(props) {
    super(props);
    this.state = { history: null};
  }

  componentDidMount() {
    axios.get(HISTORY_URL).then(response => response.data)
    .then((data) => {
        console.log(JSON.stringify(data));
      this.setState({ history: JSON.stringify(data, null, 2)})
      console.log(this.state.history)
     })
  }


  render() {
    return <div style={styles.ModalStyle}>
        <p>
         <pre>
          {this.state.history}
         </pre>
        </p>
    </div>;
  }
}

export default HistoryComponent;