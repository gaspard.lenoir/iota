# iota

## Backend

### API endpoint

List of available API at /api

    ~/counter/ (GET, POST)
    ~/history/ (GET, DELETE)
    
### Installation

In main folder

    virtualenv venv
    source venv/bin/activate
    
install package

    cd ./backend/iota
    pip install -r requirements.txt
    python manage.py migrate

### Test

    coverage run --source='.' manage.py test
    coverage report

### Execution    

Start development server

    python manage.py runserver
    
Backend should be available on http://localhost:8000/


For running in production, change value .env file :
    
    ENVIRONMENT=prod

Then 

    python manage.py collectstatic
    python manage.py runserver

## Frontend

### Installation

Install all required dependencies for frontend in rengorum/frontend folder by typing

    cd ./fronted/iota
    npm install


### Execution    

Before execution, start backend server

Start development server
    
    npm start

Frontend should be available on http://localhost:3000/
