from django.urls import path, include

from rest_framework.routers import SimpleRouter

from counter.api.views import StepViewSet, HistoryViewSet

routes = SimpleRouter()

routes.register(r"counter", StepViewSet, "counter")
routes.register(r"history", HistoryViewSet, "history")

counter_api_url = routes.urls
