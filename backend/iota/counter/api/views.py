from rest_framework import viewsets, mixins, status
from rest_framework.response import Response

from counter.models import CounterStep
from counter.api.serializer import CounterSerializer, HistorySerializer


class StepViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet):

    serializer_class = CounterSerializer

    def list(self, request):
        try:
            foo = CounterStep.objects.latest('created_at')
        except CounterStep.DoesNotExist:
            foo = CounterStep()
        serializer = CounterSerializer(foo)

        return Response(serializer.data)


class HistoryViewSet(
    mixins.ListModelMixin,
    viewsets.GenericViewSet):
    serializer_class = HistorySerializer
    pagination_class = None
    queryset = CounterStep.objects.all()

    def delete(self, request):
        CounterStep.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
